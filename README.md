# Front End

Para hacer esta capa se utilizo Polymer, esto nos permite utilizar web components.

## WebComponents
  - **my-app**: componente main, es quien contienen el template de la aplicacion y muestra los componentes asociado a los paths.
  - **login-tech-u:** componente encargado de hacer el login y persisitir en el local storage el jwt que se necesita para la autorizacion del backend.
  - **amount-value**: componente encargado de unificar la logica de redondea de los montos.
  - **accounts-tech-u**: componente encargado de invocar a la api de cuentas, para mostar las cuentas del usuario logueado. 
  - **accounts/card-accounts**: componente encargado de la visualizacion de la cuenta, se encarga de renderizar la tarjeta con la informacion.
  - **view-transactions-tech-u**: componente encargado de invocar la api de movimientos, para mostar los movimientos de una cuenta que recibe por parametro.
  - **transactions/card-transactions-day**: componente encargado de la visualizacion de los bloques de movimientos
  - **transactions/selector-transaction-type**: componente encargado hacer un selector para los distintos tipos de movimientos, se utiliza para hacer el filtrado y para la transferencia.
  - **trasfer-tech-u**: componente encargado de hacer la invocacion de la api y recabar la informacion para hacer una transferencia. 
  - **log-out**: componente encargado de la visualizacion del usuario logueado y del deslogueo.

## Componentes externos
  ### Navegacion
  Para hacer la navegacion se utiliza **iron-pages** que dependiendo de un input selected el selecciona de los componentes que puede llegar a componet el que tenga el valor seleccionado, el selected se obtiene con el componente **app-route** ya que tiene la capacidad de parsear una url la cual se obtiene de **app-location**, este ultimo da la url con la que se esta invocacion la aplicacion
  
  ### Local Storage
  Para mentener el usuario logueado y el token utilizamos **iron-localstorage** que se encarga de agregar y obtener del local storage un json por una clave.

  ### Inputs
  Para los input se utiliza **paper-input-container** que se encarga de mostar el label solo cuando el input tiene un valor seleccionado

  - Texto: **iron-input**
  - Numero: **iron-input**
  - Fecha: **vaadin-date-picker**
  - Radio Buttton: **paper-radio-button**
  - Select: **paper-dropdown-menu**
  - Boton: **paper-button**

  ### Ajax
  Para las peticiones a las apis se utiliza **iron-ajax** y para los endpoints que requieran seguridad se agrega el header de Authorization con el token que guardamos en el local storage.



